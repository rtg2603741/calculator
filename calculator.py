import math
print("Калькулятор")
a = ["1. сложение= +", "2. вычитание= -", "3. деление= /", "4. умножение= *", "5. возведение в степень= ^",
     "6. корень квадратный= √", "7. кубический корень= 3√", "8. синус= sin", "9. косинус= cos", "10. тангенс= Tg"]
print("Menu: ", *a, sep="\n")
number_1 = float(input("Введите первое число "))
operation = input("Введите операцию: ")
if operation == "+":
    number_2 = float(input("Ведите второе число "))
    print(number_1 + number_2)
elif operation == "-":
    number_2 = float(input("Ведите второе число "))
    print(number_1 - number_2)
elif operation == "/":
    number_2 = float(input("Ведите второе число "))
    if number_2 == 0:
        print("на ноль делить нельзя")
    else: print(number_1 / number_2)
elif operation == "*":
    number_2 = float(input("Ведите второе число "))
    print(number_1 * number_2)
elif operation == "^":
    s = float(input("Введите показатель степени "))
    print(number_1**s)
elif operation == "√":
    if number_1 < 0:
        print("Корень квадратный из отрицательного числа не извлекается")
    else: print(math.sqrt(number_1,))
elif operation == "3√":
    print(math.pow(number_1, 1/3))
elif operation == "sin":
    print(math.sin(number_1))
elif operation == "cos":
    print(math.cos(number_1))
elif operation == "Tg":
    print(math.tan(number_1))
else: print("Такого действия нет")